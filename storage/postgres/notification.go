package postgres

import (
	"database/sql"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.udevs.io/notification_service/pkg/helper"
	"gitlab.udevs.io/notification_service/storage/repo"
	"gitlab.udevs.io/notification_service/variables"
)

type notificationRepo struct {
	db *sqlx.DB
}

func NewNotificationRepo(db *sqlx.DB) repo.NotificationStorageI {
	return &notificationRepo{db: db}
}

func (nr *notificationRepo) Create(notification *variables.CreateNotificationRequest) (string, error) {
	var orderId sql.NullString

	noteId, err := uuid.NewRandom()

	if err != nil {
		return "", err
	}

	orderId = helper.ToNullString(notification.OrderId)

	tx, err := nr.db.Begin()

	if err != nil {
		return "", err
	}

	_, err = tx.Exec(
		`INSERT INTO notifications(
			id,
			content,
			celebrity_id,
			sender_id,
			sender_type,
			order_id)
			values ($1, $2, $3, $4, $5)`,
		noteId,
		notification.Content,
		notification.CelebrityId,
		notification.SenderId,
		notification.SenderType,
		orderId,
	)

	if err != nil {
		tx.Rollback()
		return "", err
	}

	_, err = tx.Exec(
		`INSERT INTO notif_receivers(id, notif_id)
		VALUES ($1, $2, $3)`, notification.CelebrityId, noteId)

	return "", nil
}
