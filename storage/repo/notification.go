package repo

import "gitlab.udevs.io/notification_service/variables"

type NotificationStorageI interface {
	Create(notification *variables.CreateNotificationRequest) (string, error)
}
