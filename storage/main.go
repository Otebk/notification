package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.udevs.io/notification_service/storage/postgres"
	"gitlab.udevs.io/notification_service/storage/repo"
)

type StorageI interface {
	Notification() repo.NotificationStorageI
}

type storagePg struct {
	db               *sqlx.DB
	notificationRepo repo.NotificationStorageI
}

func NewStoragePg(db *sqlx.DB) StorageI {
	return &storagePg{
		db:               db,
		notificationRepo: postgres.NewNotificationRepo(db),
	}
}

func (s *storagePg) Notification() repo.NotificationStorageI {
	return s.notificationRepo
}
