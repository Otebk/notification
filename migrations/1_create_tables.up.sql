CREATE TABLE IF NOT EXISTS notifications(
    id UUID PRIMARY KEY,
    sender_id UUID NOT NULL,
    celebrity_id UUID NOT NULL,
    content TEXT NOT NULL ,
    sender_type VARCHAR(20) NOT NULL,
    order_id UUID,
    created_at TIMESTAMP NOT NULL DEFAULT current_timestamp
);

CREATE TABLE IF NOT EXISTS notif_receiver(
    id UUID PRIMARY KEY,
    notif_id UUID REFERENCES notifications(id) NOT NULL
);