package config

import (
	"os"

	"github.com/spf13/cast"
)

type Config struct {
	Environment string

	PostgresHost     string
	PostgresPort     int
	PostgresUser     string
	PostgresPassword string
	PostgresDatabase string

	RabbitMQHost     string
	RabbitMQPort     int
	RabbitMQUser     string
	RabbitMQPassword string

	RedisHost             string
	RedisPort             int
	RedisTimeoutInSeconds int

	ExchangeName string

	SmsRouteID string

	SigningKey []byte

	LogLevel string
	HttpPort string
}

func Load() Config {
	config := Config{}
	config.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "develop"))

	config.PostgresHost = cast.ToString(getOrReturnDefault("POSTGRES_HOST", "localhost"))
	config.PostgresPort = cast.ToInt(getOrReturnDefault("POSTGRES_PORT", 5432))
	config.PostgresUser = cast.ToString(getOrReturnDefault("POSTGRES_USER", "postgres"))
	config.PostgresPassword = cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "123"))
	config.PostgresDatabase = cast.ToString(getOrReturnDefault("POSTGRES_DATABASE", "muno_notif_service"))

	config.RabbitMQHost = cast.ToString(getOrReturnDefault("RABBITMQ_HOST", "localhost"))
	config.RabbitMQPort = cast.ToInt(getOrReturnDefault("RABBITMQ_PORT", 5672))
	config.RabbitMQUser = cast.ToString(getOrReturnDefault("RABBITMQ_USER", "admin"))

	config.RabbitMQPassword = cast.ToString(getOrReturnDefault("RABBITMQ_PASSWORD", "admin"))

	config.RedisHost = cast.ToString(getOrReturnDefault("REDIS_HOST", "localhost"))
	config.RedisPort = cast.ToInt(getOrReturnDefault("REDIS_PORT", "6379"))
	config.RedisTimeoutInSeconds = cast.ToInt(getOrReturnDefault("REDIS_TIMEOUT_IN_SECONDS", 120))

	config.ExchangeName = cast.ToString(getOrReturnDefault("EXCHANGE_NAME", "v1.muno"))

	config.SmsRouteID = cast.ToString(getOrReturnDefault("SMS_ROUTE_ID", "5a399064-8ef7-4756-8790-fcaf7f4239f9"))

	config.SigningKey = []byte(cast.ToString(getOrReturnDefault("SIGNING_KEY", "aSk0bynVpMa2l5xc0a9ri8IjYF/CaJPa7lttFMIt+2qcbZjsWWfEISfTAXD+DfkwwpysH7I7DQKvWcehqVgmrA==")))

	config.HttpPort = cast.ToString(getOrReturnDefault("HTTP_PORT", ":5002"))
	return config
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}

	return defaultValue
}
