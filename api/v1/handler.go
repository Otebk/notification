package v1

import (
	"github.com/streadway/amqp"
	"gitlab.udevs.io/notification_service/config"
	"gitlab.udevs.io/notification_service/pkg/logger"
)

type handlerV1 struct {
	log logger.Logger
	cfg *config.Config
	//storage storage.StorageI
	//redis   redis.Conn
	channel *amqp.Channel
}

type HandlerV1Options struct {
	Log logger.Logger
	Cfg *config.Config
	//Storage storage.StorageI
	//Redis   redis.Conn
	Channel *amqp.Channel
}

func New(options *HandlerV1Options) *handlerV1 {
	return &handlerV1{
		log: options.Log,
		cfg: options.Cfg,
		//storage: options.Storage,
		//redis:   options.Redis,
		channel: options.Channel,
	}
}
