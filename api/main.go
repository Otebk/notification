package api

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/streadway/amqp"
	v1 "gitlab.udevs.io/notification_service/api/v1"
	"gitlab.udevs.io/notification_service/config"
	"gitlab.udevs.io/notification_service/pkg/logger"
)

type RouterOptions struct {
	Log logger.Logger
	Cfg *config.Config
	//Storage storage.StorageI
	//Redis   redis.Conn
	Channel *amqp.Channel
}

func New(opt *RouterOptions) *gin.Engine {
	router := gin.New()
	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	cfg := cors.DefaultConfig()

	cfg.AllowHeaders = append(cfg.AllowHeaders, "*")
	cfg.AllowAllOrigins = true
	cfg.AllowCredentials = true

	router.Use(cors.New(cfg))

	handlerV1 := v1.New(&v1.HandlerV1Options{
		Log: opt.Log,
		Cfg: opt.Cfg,
		//Storage: opt.Storage,
		//Redis:   opt.Redis,
		Channel: opt.Channel,
	})

	apiV1 := router.Group("/v1")

	apiV1.POST("notification", handlerV1.SendNotification)

	return router
}
