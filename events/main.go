package events

import (
	"context"

	"gitlab.udevs.io/notification_service/config"
	"gitlab.udevs.io/notification_service/events/notification"
	"gitlab.udevs.io/notification_service/pkg/event"
	"gitlab.udevs.io/notification_service/pkg/helper"
	"gitlab.udevs.io/notification_service/pkg/logger"
)

type PubSubServer struct {
	cfg config.Config
	log logger.Logger
	RMQ *event.RMQ
}

// New ...
func New(cfg config.Config, log logger.Logger) (*PubSubServer, error) {
	rmqURI := helper.GetRabbitMQURI(
		cfg.RabbitMQUser,
		cfg.RabbitMQPassword,
		cfg.RabbitMQHost,
		cfg.RabbitMQPort,
	)

	rmq, err := event.NewRMQ(rmqURI, log)

	if err != nil {
		return nil, err
	}

	return &PubSubServer{
		cfg: cfg,
		log: log,
		//db:  db,
		RMQ: rmq,
	}, nil
}

func (ps *PubSubServer) Run(ctx context.Context) {
	notificationService := notification.New(ps.cfg, ps.log, ps.RMQ)
	notificationService.RegisterConsumers()

	ps.RMQ.RunConsumers(ctx)
}
