package notification

import (
	"bytes"
	//"context"
	"fmt"

	"io/ioutil"
	//"log"
	"net/http"

	//"github.com/appleboy/gorush/rpc/proto"

	"github.com/streadway/amqp"
	"gitlab.udevs.io/notification_service/variables"
	//"google.golang.org/grpc"
	//"google.golang.org/protobuf/types/known/structpb"
)

// func (n *NotificationService) Create(delivery amqp.Delivery) variables.Response {
// 	url := "https://fcm.googleapis.com/fcm/send"
// 	fmt.Println("URL:>", url)
// 	var resp variables.Response
// 	var jsonStr = []byte(`{
// 		"to":"/topics/Lebazar",
// 		"notification" : {
// 		"title": "Test",
// 		"body" : "Yangi taomlar ok",
// 		"click_action": "FLUTTER_NOTIFICATION_CLICK"}
// 	}`)
// 	conn, err := grpc.Dial(url, grpc.WithInsecure())

// 	if err != nil {
// 		log.Fatal("did not connect", err)
// 		resp = variables.Response{
// 			ID:         "",
// 			StatusCode: http.StatusBadRequest,
// 			Error: variables.Error{
// 				Message: "error while unamrshaling",
// 				Reason:  err.Error(),
// 			},
// 		}
// 		return resp
// 	}
// 	defer conn.Close()

// 	c := proto.NewGorushClient(conn)

// 	r, err := c.Send(context.Background(), &proto.NotificationRequest{
// 		Alert:   &proto.Alert{},
// 		Message: string(jsonStr),
// 		Data: &structpb.Struct{
// 			Fields: map[string]*structpb.Value{
// 				"Authorization": {
// 					Kind: &structpb.Value_StringValue{StringValue: "key=AAAAExn0abM:APA91bEApZqWZg8g2gHD4_82DJOjW-uXHTPMCCLa2Fe4C_ydKW1NUe_I4t9QeGk7JTPWSTd6BmvA-tnUJ5HEq0hEA0yfoLopD1NSXhGacs9KkOBx8DDE-Pb3hDYrmqh-x4dwJxaNmiUK"},
// 				},
// 				"Content-type": {
// 					Kind: &structpb.Value_StringValue{StringValue: "application/json"},
// 				},
// 			},
// 		},
// 	})
// 	if err != nil {
// 		log.Fatalf("could not greet: %v", err)
// 	}
// 	log.Printf("Success: %t\n", r.Success)
// 	log.Printf("Count: %d\n", r.Counts)
// 	resp = variables.Response{
// 		StatusCode: http.StatusOK,
// 	}
// 	return resp
// }

func (n *NotificationService) Create(delivery amqp.Delivery) variables.Response {
	url := "https://fcm.googleapis.com/fcm/send"
	fmt.Println("URL:>", url)

	var jsonStr = []byte(`{
		"to":"/topics/Lebazar",
		"notification" : {
		"title": "Test",
		"body" : "Yangi taomlar ok",
		"click_action": "FLUTTER_NOTIFICATION_CLICK"}
	}`)

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Authorization", "key=AAAAExn0abM:APA91bEApZqWZg8g2gHD4_82DJOjW-uXHTPMCCLa2Fe4C_ydKW1NUe_I4t9QeGk7JTPWSTd6BmvA-tnUJ5HEq0hEA0yfoLopD1NSXhGacs9KkOBx8DDE-Pb3hDYrmqh-x4dwJxaNmiUK")
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("response Body:", string(body))
	response := variables.Response{
		StatusCode: http.StatusOK,
	}
	return response
}
