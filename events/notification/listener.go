package notification

import (
	"gitlab.udevs.io/notification_service/config"
	"gitlab.udevs.io/notification_service/pkg/event"
	"gitlab.udevs.io/notification_service/pkg/logger"
	"gitlab.udevs.io/notification_service/variables"
)

type NotificationService struct {
	cfg config.Config
	log logger.Logger
	//storage storage.StorageI
	rmq *event.RMQ
}

func New(cfg config.Config, log logger.Logger, rmq *event.RMQ) *NotificationService {
	return &NotificationService{
		cfg: cfg,
		log: log,
		//storage: storage.NewStoragePg(db),
		rmq: rmq,
	}
}

func (c *NotificationService) RegisterConsumers() {
	notif := variables.NotificationServiceRoute

	c.rmq.NewConsumer(
		notif+"create",         // consumerName
		variables.ExchangeName, // exchangeName
		notif+"create",         // queueName
		notif+"create",         // routingKey
		c.Create,               // handlerFunction
	)
}
