package variables

type Response struct {
	SessionID  string `json:"session_id"`
	ID         string `json:"id"`
	StatusCode int    `json:"response_code"`
	Error      Error  `json:"error"`
}
