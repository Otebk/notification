package variables

const (
	ExchangeName        = "v1.muno"
	WebSocketRoutingKey = "v1.response"
)

var (
	NotificationServiceRoute = "v1.notification."
)
