package variables

type CreateNotificationRequest struct {
	ID          string `json:"id"`
	Content     string `json:"content"`
	SenderId    string `json:"sender_id"`
	CelebrityId string `json:"celebrity_id"`
	SenderType  string `json:"sender_type"`
	OrderId     string `json:"order_id"`
}
