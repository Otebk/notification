package event

import (
	"context"
	"sync"

	"gitlab.udevs.io/notification_service/pkg/logger"

	"github.com/streadway/amqp"
)

// RMQ ...
type RMQ struct {
	log            logger.Logger
	amqpURI        string
	conn           *amqp.Connection
	connErr        chan *amqp.Error
	consumers      map[string]*Consumer
	consumerErrors chan error
	publishers     map[string]*Publisher
}

// NewRMQ ...
func NewRMQ(amqpURI string, log logger.Logger) (*RMQ, error) {
	conn, err := amqp.Dial(amqpURI)
	if err != nil {
		return nil, err
	}

	rmq := &RMQ{
		conn:           conn,
		amqpURI:        amqpURI,
		log:            log,
		connErr:        make(chan *amqp.Error),
		consumers:      make(map[string]*Consumer),
		consumerErrors: make(chan error),
		publishers:     make(map[string]*Publisher),
	}

	rmq.conn.NotifyClose(rmq.connErr)

	go rmq.receiveConnErr() // shows connections errors in loop

	go rmq.receiveConsumerError() // shows consumer errors in loop

	return rmq, nil
}

func (rmq *RMQ) RunConsumers(ctx context.Context) {
	var wg sync.WaitGroup

	for _, consumer := range rmq.consumers {
		wg.Add(1)
		go func(wg *sync.WaitGroup, c *Consumer) {
			defer wg.Done()
			c.Start(ctx)
		}(&wg, consumer)
		// fmt.Println("Key:", consumer.queueName, "=>", "consumer:", consumer)
	}

	wg.Wait()
}

func (rmq *RMQ) receiveConsumerError() {
	for err := range rmq.consumerErrors {
		rmq.log.Error("consumer error", logger.Error(err))
	}
}

func (rmq *RMQ) receiveConnErr() {
	for err := range rmq.connErr {
		rmq.log.Fatal("connection error", logger.Error(err))
	}
}
