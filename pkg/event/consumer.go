package event

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"

	"github.com/streadway/amqp"
	"gitlab.udevs.io/notification_service/variables"
)

type HandlerFunc func(amqp.Delivery) variables.Response

// Consumer ...
type Consumer struct {
	consumerName string
	channel      *amqp.Channel
	exchangeName string
	routingKey   string
	queueName    string
	handler      HandlerFunc
	messages     <-chan amqp.Delivery
	errors       chan error
}

// NewConsumer ...
func (rmq *RMQ) NewConsumer(consumerName, exchangeName, routingKey, queueName string, handler func(amqp.Delivery) variables.Response) error {
	if rmq.consumers[consumerName] != nil {
		return errors.New("consumer with the same name already exists: " + consumerName)
	}

	ch, err := rmq.conn.Channel()

	if err != nil {
		return err
	}

	err = declareExchange(ch, exchangeName)

	if err != nil {
		fmt.Printf("Exchange Declare: %s", err.Error())
		return err
	}

	q, err := declareQueue(ch, queueName)

	if err != nil {
		return err
	}

	err = ch.QueueBind(
		q.Name,
		routingKey,
		exchangeName,
		false,
		nil,
	)

	if err != nil {
		return err
	}

	messages, err := ch.Consume(
		q.Name,
		consumerName,
		false,
		false,
		false,
		true,
		nil,
	)

	if err != nil {
		panic(err)
	}

	rmq.consumers[consumerName] = &Consumer{
		consumerName: consumerName,
		channel:      ch,
		exchangeName: exchangeName,
		routingKey:   routingKey,
		queueName:    q.Name,
		handler:      handler,
		messages:     messages,
		errors:       rmq.consumerErrors,
	}

	return nil
}

// Start ...
func (c *Consumer) Start(ctx context.Context) {
	log.Println("Consumer started for: ", c.queueName)

	for {
		select {
		case msg, ok := <-c.messages:
			if !ok {
				panic(errors.New("error while reading consumers messages"))
				// c.errors <- errors.New("error while reading consumer messages " + c.queueName)
				// time.Sleep(time.Duration(5000 * time.Millisecond))
			} else {
				resp := c.handler(msg)
				log.Println("Response: ", resp)
				c.pushToSocket(resp)
			}
		case <-ctx.Done():
			{
				err := c.channel.Cancel("", true)
				if err != nil {
					c.errors <- err
				}
				return
			}
		}
	}
}

func (c *Consumer) pushToSocket(msg variables.Response) {
	byteBody, err := json.Marshal(msg)
	if err != nil {
		log.Println("Error while marshalling into byte from json", err.Error())
		return
	}

	err = c.channel.Publish(
		variables.ExchangeName,        // exchange name
		variables.WebSocketRoutingKey, // routing key
		false,                         // mandatory
		false,                         // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        byteBody,
		})

	if err != nil {
		c.errors <- err
	}
}
