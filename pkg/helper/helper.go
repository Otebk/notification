package helper

import (
	"database/sql"
	"fmt"
)

func GetRabbitMQURI(user, password, host string, port int) string {
	return fmt.Sprintf("amqp://%s:%s@%s:%d", user, password, host, port)
}

func ToNullString(s string) (ns sql.NullString) {
	if s != "" {
		ns.String = s
		ns.Valid = true
	}
	return ns
}
