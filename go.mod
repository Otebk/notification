module gitlab.udevs.io/notification_service

go 1.15

require (
	github.com/appleboy/gorush v1.13.0
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.7.1
	github.com/google/uuid v1.2.0
	github.com/jmoiron/sqlx v1.3.3
	github.com/spf13/cast v1.3.1
	github.com/streadway/amqp v1.0.0
	go.uber.org/zap v1.16.0
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e
	google.golang.org/grpc v1.37.0
)
