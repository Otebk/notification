package main

import (
	"context"
	"fmt"

	"github.com/streadway/amqp"
	"gitlab.udevs.io/notification_service/api"
	"gitlab.udevs.io/notification_service/config"
	"gitlab.udevs.io/notification_service/events"
	"gitlab.udevs.io/notification_service/pkg/helper"
	"gitlab.udevs.io/notification_service/pkg/logger"
	"golang.org/x/sync/errgroup"
)

func main() {
	cfg := config.Load()
	log := logger.New(cfg.LogLevel, "notification_service")
	pubsubServer, err := events.New(cfg, log)
	if err != nil {
		panic(err)
	}
	group, ctx := errgroup.WithContext(context.Background())
	group.Go(func() error {
		pubsubServer.Run(ctx)
		panic("error to connect pubsubserver")
	})
	rabbitMQUrl := helper.GetRabbitMQURI(cfg.RabbitMQUser, cfg.RabbitMQPassword, cfg.RabbitMQHost, cfg.RabbitMQPort)

	rabbitMQConn, err := amqp.Dial(rabbitMQUrl)

	if err != nil {
		panic(err)
	}
	channel, err := rabbitMQConn.Channel()
	if err != nil {
		panic(err)
	}
	group.Go(func() error {
		apiserver := api.New(&api.RouterOptions{
			Log:     log,
			Cfg:     &cfg,
			Channel: channel,
		})

		err := apiserver.Run(cfg.HttpPort)

		if err != nil {
			panic(fmt.Sprintf("error on the api server: %v", err))
		}
		panic("api server has finished")
	})

	err = group.Wait()
	if err != nil {
		panic("error on the server")
	}
}
