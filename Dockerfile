# workspace (GOPATH) configured at /go
FROM golang:1.15 as builder


#
RUN mkdir -p $GOPATH/src/gitlab.udevs.io/muno/muno_notification_service
WORKDIR $GOPATH/src/gitlab.udevs.io/muno/muno_notification_service

# Copy the local package files to the container's workspace.
COPY . ./

# installing depends and build
RUN export CGO_ENABLED=0 && \
    export GOOS=linux && \
    make build && \
    mv ./bin/muno_user_service /

ENTRYPOINT ["/muno_notification_service"]
